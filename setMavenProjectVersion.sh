#!/bin/bash

commit_date=$(git log -n 1 --pretty='format:%cd' --date=format:'%y%m%d')

commit_hash=$(git rev-parse --short HEAD)

version_info="$commit_date-$commit_hash"

cd target && mv assignment-*.jar $version_info.jar